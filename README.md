# Güvenli Harita
Yaşanabilecek bir deprem sonrası insanların güvenli bölgelere erişimini kolaylaştırmayı amaçlayan bir web uygulamasıdır. 

## Linkler
- Web Uygulaması - [https://guvenliharita.com](https://guvenliharita.com/) 
- Api Servisi - [https://api.guvenliharita.com](https://api.guvenliharita.com/) 

## Yapılacaklar Listesi
- Kullanıcı uygulamayı açtığında mevcut konum bilgisi alınmalı.
- Uygulama içerisinde bir map bulunmalı, ve alınan konum bilgisine göre (mevcut konum) haritada işaretlenmeli.
- İlgili api çağırımı ile Şehir verileri çekilmeli.
- Çekilen verinin bir dropdown widget'ı içerisinde listelenmeli ve kullanıcı şehir seçimi yapabilmeli.
- Şehir seçimi yapıldıktan sonra ilgili şehir Id'si ile CommunityCenter çağırımı yapılmalı.
- Alınan CommunityCenter verilerinin 'longitude' ve 'latitude' değerlerine göre harita üzerinde gösterilmeli.
- Kullanıcı yol tarifi alabilmesi için, harita üzerinde işaretlenen CommunityCenter'larından birisine tıklar ve ilgili CommunityCenter'ın 'longitude' ve 'latitude' değerleri ile telefonda bulunan bir harita uygulamasına yönlendirilir. (Maps, Google Maps)

## API Kullanımı

### City

```http
  GET https://api.guvenliharita.com/api/services/app/City/GetAllForComboBox?isAll=true
```

| Parametre | Tip     | Açıklama                |
| :-------- | :------- | :------------------------- |
| `isAll` | `boolean` | false ise sadece CommunityCenter bulunan şehirleri, true ise tüm şehirleri getirir. |

Şuan için sadece Şanlıurfa şehri için CommunityCenter verileri mevcut.

### CommunityCenter 

```http
  GET https://api.guvenliharita.com/api/services/app/CommunityCenter/GetAllForCity?cityId=${cityId}
```

| Parametre | Tip     | Açıklama                       |
| :-------- | :------- | :-------------------------------- |
| `cityId`      | `int` | CommunityCenter'larının listelenmesi istenilen Şehrin Id'si |

#### Örnek
Şanlıurfa şehrine ait CommunityCenter'larını çekmek için endpoint örneği:

https://api.guvenliharita.com/api/services/app/CommunityCenter/GetAllForCity?cityId=63 
